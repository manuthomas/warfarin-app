package com.example.amruth.warfarinpredictorclient;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PredAdapter extends RecyclerView.Adapter<PredAdapter.predViewholder> {

    private Context context;
    List<PredictionList> dosageList;

    public PredAdapter(Context context, List<PredictionList> dosageList) {
        this.context = context;
        this.dosageList = dosageList;
    }

    @NonNull
    @Override
    public predViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_layout,viewGroup,false);
        return new predViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull predViewholder predViewholder, int i) {

        PredictionList predDosage = dosageList.get(i);
        predViewholder.dateText.setText(predDosage.getDate());
        predViewholder.dayText.setText(predDosage.getDay());
        predViewholder.valuePred.setText(predDosage.getValue().toString());
    }

    @Override
    public int getItemCount() {
        return dosageList.size();
    }

    class predViewholder extends RecyclerView.ViewHolder{

        TextView dayText,dateText,valuePred;

        public predViewholder(@NonNull View itemView) {
            super(itemView);

            dayText = itemView.findViewById(R.id.dayText);
            dateText = itemView.findViewById(R.id.dateText);
            valuePred = itemView.findViewById(R.id.valuePred);

        }
    }
}
