package com.example.amruth.warfarinpredictorclient;

public class PredictionList {
    private String day, date;
    private Integer value;

    public PredictionList(String day, String date, Integer value) {
        this.day = day;
        this.date = date;
        this.value = value;
    }

    public String getDay() {
        return day;
    }

    public String getDate() {
        return date;
    }

    public Integer getValue() {
        return value;
    }
}
