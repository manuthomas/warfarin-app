package com.example.amruth.warfarinpredictorclient.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.amruth.warfarinpredictorclient.R;
import com.example.amruth.warfarinpredictorclient.patient.Patient;
import com.example.amruth.warfarinpredictorclient.utils.RequestQueueSingleton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;


//TODO: Convert url encoding to JSON request
public class LoginActivity extends AppCompatActivity {


    public static final String lOGAPPENDER = "WarfarinPredictorClient";
    int flag=0;
    int clickCount=0;
    Button btn;
    private EditText userLog;
    private TextInputLayout userlogBox;
    private EditText passlog;
    private TextInputLayout passLogBox;
    private Button logBut;


    private RelativeLayout loadLay;

    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userLog = (EditText) findViewById(R.id.Username1);
        userlogBox = (TextInputLayout) findViewById(R.id.Username);
        passlog = (EditText) findViewById(R.id.Password1);
        passLogBox = (TextInputLayout) findViewById(R.id.Password);
        logBut = (Button) findViewById(R.id.loginButton);

        userLog.setNextFocusDownId(R.id.Password1);
        passlog.setNextFocusDownId(R.id.loginButton);

        loadLay = (RelativeLayout)findViewById(R.id.loadViewId);

        loadLay.setVisibility(View.GONE);

        boolean netcheck=isNetworkConnected();
        Log.i(lOGAPPENDER,"Net has been checked");
        if(!netcheck){
            Log.i(lOGAPPENDER,"No internet");
            Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show();
            Log.i(lOGAPPENDER,"Dialog created n shown");
        }
    }

    @Override
    public void onBackPressed() {
        //do nothing
    }

    public void disclaimerAlert(final Intent intent){


        String disclaimerText = "This app is designed to be used only in conjunction with the physician prescription and medical advice you receive and should not be used as a method of self-prescribing medication. The information generated is not medical advice or prescription and the developers of this App have no medico-legal liability in whatever consequences related to its use.  By using this App you are agreeing to our terms and conditions, and our disclaimer of its use.";
        new AlertDialog.Builder(this)
                .setTitle("Disclaimer")
                .setMessage(disclaimerText)
                .setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        loadLay.setVisibility(View.GONE);
                    }
                })
                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(intent);
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                loadLay.setVisibility(View.GONE);
            }
        })
                .show();
    }

    public void requestLogin(View view) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        boolean netcheck=isNetworkConnected();
        Log.i(lOGAPPENDER,"Net has been checked");
        if(!netcheck){
            loadLay.setVisibility(View.GONE);
            Log.i(lOGAPPENDER,"No internet");
            Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show();
            Log.i(lOGAPPENDER,"Dialog created n shown");
        }
        else{

        final ImmutableMap<String,String> loginCredentials = getLoginCredentials();

        if (flag == 1){
            return;
        }

        loadLay.setVisibility(View.VISIBLE);


        RequestQueue loginRequestQueue = RequestQueueSingleton.getInstance(this.getApplicationContext())
                .getRequestQueue();

        String authenticationServerURL = "https://warfarin-predictor.herokuapp.com/predictor/api-token-auth";
        StringRequest loginRequest = new StringRequest(Request.Method.POST,
                authenticationServerURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(lOGAPPENDER, response);
                Gson gson = new Gson();
                HashMap<String,String> loginResponse = gson.fromJson(response,new TypeToken<HashMap<String,String>>(){}.getType());
                Log.d(lOGAPPENDER,"Authorization token " + loginResponse.get("token"));
                Patient.getInstance().setAuthorizationToken(loginResponse.get("token"));
                Intent intent = new Intent(getBaseContext(),MainActivity.class);

                intent.putExtra("user",getUserNameFromUI());

                //Displaying Disclaimer before login in
                disclaimerAlert(intent);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d(lOGAPPENDER,"Error logging in");
                displayInvalidLoginError(error);
                loadLay.setVisibility(View.GONE);
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }

            @Override
            public Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", loginCredentials.get("username"));
                params.put("password", loginCredentials.get("password"));
                return params;
            }


        };


        loginRequestQueue.add(loginRequest);
    }}



    private Toast mToastToShow;
    public void showToast(View view) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = 10000;
        mToastToShow = Toast.makeText(this, "Invalid username or password", Toast.LENGTH_LONG);

        // Set the countdown to display the toast
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }
            public void onFinish() {
                mToastToShow.cancel();
            }
        };

        // Show the toast and starts the countdown
        mToastToShow.show();
        toastCountDown.start();
    }

    


    private void displayInvalidLoginError(VolleyError error){
        if(error instanceof TimeoutError){
            Toast toast = Toast.makeText(getBaseContext(),"Request timed out, please try again",Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        else {
           Toast toast = Toast.makeText(getBaseContext(), "Invalid username or password", Toast.LENGTH_LONG);
           toast.show();

          //  View v=findViewById(android.R.id.content).getRootView();
          //  showToast(v);
            return;

        }
    }

    private ImmutableMap<String, String> getLoginCredentials() {
        String username = getUserNameFromUI();
        String password = getPasswordFromUI();
        if (username.length() < 5 || password.length() < 5 || username.contains(" ")){

            flag = 1;

            if (username.length() < 4){
                userlogBox.setError("Username needs to be at least 4 characters.");
            }
            else if(username.contains(" ")){
                userlogBox.setError("Spaces are not allowed");
            }
            else{
                userlogBox.setErrorEnabled(false);
            }
            if(password.length() < 4){
                passLogBox.setError("Password needs to be at least 4 characters.");
            }
            else{
                passLogBox.setErrorEnabled(false);
            }
            /*
            Intent i=new Intent(LoginActivity.this,LoginActivity.class);
            finish();
            overridePendingTransition(0,0);
            startActivity(i);
            overridePendingTransition(0,0);

             */
            loadLay.setVisibility(View.GONE);
        }else{
            flag = 0;
            userlogBox.setErrorEnabled(false);
            passLogBox.setErrorEnabled(false);
        }

        ImmutableMap<String, String> credentails = buildCredentialsMap(username, password);
        return credentails;
    }

    private ImmutableMap<String, String> buildCredentialsMap(String username, String password) {
        ImmutableMap<String,String> credentials = new ImmutableMap.Builder<String,String>()
                .put("username",username)
                .put("password",password)
                .build();
        return  credentials;
    }

    private String getPasswordFromUI() {
        return passlog.getText().toString();
    }

    private String getUserNameFromUI() {
        return userLog.getText().toString();
    }
    private boolean isNetworkConnected() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return connected;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {

            case KeyEvent.KEYCODE_ENTER:
                boolean netcheck=isNetworkConnected();
                Log.i(lOGAPPENDER,"Net has been checked");
                if(!netcheck){
                    Log.i(lOGAPPENDER,"No internet");
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage("Net Connection Is Not Present");
                    builder.setPositiveButton("Refresh", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(getBaseContext(),LoginActivity.class);
                            startActivity(intent);
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    Log.i(lOGAPPENDER,"Dialog created n shown");
                }

                else{

                    clickCount++;
                    if (clickCount==1){
                        btn= (Button) findViewById(R.id.loginButton);
                        btn.setVisibility(View.GONE);
                    }
                    if (flag==0){

                        final ImmutableMap<String,String> loginCredentials = getLoginCredentials();

                        RequestQueue loginRequestQueue = RequestQueueSingleton.getInstance(this.getApplicationContext())
                                .getRequestQueue();

                        String authenticationServerURL = "https://warfarin-predictor.herokuapp.com/predictor/api-token-auth";
                        StringRequest loginRequest = new StringRequest(Request.Method.POST,
                                authenticationServerURL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(lOGAPPENDER, response);
                                Gson gson = new Gson();
                                HashMap<String,String> loginResponse = gson.fromJson(response,new TypeToken<HashMap<String,String>>(){}.getType());
                                Log.d(lOGAPPENDER,"Authorization token " + loginResponse.get("token"));
                                Patient.getInstance().setAuthorizationToken(loginResponse.get("token"));
                                Intent intent = new Intent(getBaseContext(),MainActivity.class);

                                intent.putExtra("user",getUserNameFromUI());

                                startActivity(intent);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                Log.d(lOGAPPENDER,"Error logging in");
                                displayInvalidLoginError(error);
                                btn= (Button) findViewById(R.id.loginButton);
                                btn.setVisibility(View.GONE);
                            }
                        })
                        {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Content-Type","application/x-www-form-urlencoded");
                                return params;
                            }

                            @Override
                            public Map<String,String> getParams(){
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("username", loginCredentials.get("username"));
                                params.put("password", loginCredentials.get("password"));
                                return params;
                            }


                        };


                        loginRequestQueue.add(loginRequest);}}
                return true;


            default:
                return super.onKeyDown(keyCode, event);
        }
    }

}
