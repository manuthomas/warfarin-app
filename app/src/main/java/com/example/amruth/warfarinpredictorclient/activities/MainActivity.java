package com.example.amruth.warfarinpredictorclient.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.amruth.warfarinpredictorclient.R;
import com.example.amruth.warfarinpredictorclient.patient.Patient;
import com.example.amruth.warfarinpredictorclient.patient.PatientMedicalData;
import com.example.amruth.warfarinpredictorclient.utils.RequestQueueSingleton;
import com.example.amruth.warfarinpredictorclient.patient.PatientWarfarinMedicalData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/*
class NetworkCheck implements Runnable {

    NetworkCheck(long minTime) {
        this.minTime = minTime;
    }

    public void run() {
        new CountDownTimer(30000, 1000){
            public void onTick(long millisUntilFinished){
                boolean connected = false;
                ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                    //we are connected to a network
                    connected = true;
                }
                else
                    connected = false;
            }
            public  void onFinish(){
                textView.setText("FINISH!!");
            }
        }.start();

    }
}

    NetworkCheck nw = new NetworkCheck(143);
     new Thread(nw).start();
*/
public class MainActivity extends AppCompatActivity {


    /*
    String age= getIntent().getExtras().getString("age");
    EditText ageInput = (EditText)findViewById(R.id.patient_age);
    ageInput.setText();
*/
    public static int patientAge=0;
    public static float oldINRValue=0;
    public static float newINRValue=0;
    public static float oldDosage=0;
    private static int position = 0;
    private static int drugPosition = 0;
    private static String drug = "";
    private static String patientID = "";
    private RelativeLayout loadMain;
    private TextInputLayout ageBox;
    private TextInputLayout idBox;
    private TextInputLayout newInrBox;
    private TextInputLayout oldInrBox;
    private TextInputLayout dosageBox;
    private static String prevDosages = "";



    int flag=0;
    private String logAppender = "WarfarinPredictorClient";

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadMain = (RelativeLayout)findViewById(R.id.loadMainId);
        loadMain.setVisibility(View.GONE);

        ageBox = (TextInputLayout) findViewById(R.id.patient_age_box);
        newInrBox = (TextInputLayout) findViewById(R.id.patient_newINR_box);
        oldInrBox = (TextInputLayout) findViewById(R.id.patient_oldINR_box);
        dosageBox = (TextInputLayout) findViewById(R.id.patient_previous_dosage_box);
        idBox = (TextInputLayout) findViewById(R.id.patient_id_box);
        TabLayout mTabLayout = (TabLayout)findViewById(R.id.genderTab);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                position = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        TabLayout dTabLayout = (TabLayout)findViewById(R.id.drugTab);

        dTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                drugPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        flag=0;

        EditText idInput = (EditText)findViewById(R.id.patient_id);

        if (idInput.getText().equals("")){
            patientID="";
        }

        EditText ageInput = (EditText)findViewById(R.id.patient_age);

        if (ageInput.getText().equals("")){
            patientAge=0;
        }
        EditText oldINRInput = (EditText)findViewById(R.id.patient_oldINR);

        if (oldINRInput.getText().equals("")){
            oldINRValue=0;
        }

        EditText newINRInput = (EditText)findViewById(R.id.patient_newINR);

        if (newINRInput.getText().equals("")){
            newINRValue=0;
        }

        EditText oldDosageInput = (EditText)findViewById(R.id.patient_previous_dosage);

        if (oldDosageInput.getText().equals("")){
            oldDosage=0;
        }



        boolean netcheck=isNetworkConnected();
        if(!netcheck){
            Log.i(getLogAppender(),"No internet");
            Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show();
            Log.i(getLogAppender(),"Dialog created n shown");
        }
        else{
            try{
            String user=getIntent().getExtras().getString("user");
            if (user.equals("null")) {
                Log.i(logAppender, "Null user value");
            }
            else{Log.i(logAppender,"Obtained user name "+user);

            Toast welcome = Toast.makeText(getApplicationContext(),"Welcome "+user,Toast.LENGTH_LONG );
            welcome.show();}}
            catch (Exception e){Log.i(logAppender,"Null user value");}

            idInput = (EditText)findViewById(R.id.patient_id);
            if (patientID==""){
                idInput.setText("");
            }else{
                idInput.setText(patientID);
            }

            ageInput = (EditText)findViewById(R.id.patient_age);
            if (patientAge==0){
                ageInput.setText("");
            }
            else{
                ageInput.setText(patientAge+"");
            }
            oldINRInput = (EditText)findViewById(R.id.patient_oldINR);
            if (oldINRValue==0){
                oldINRInput.setText("");
            }
            else{
                oldINRInput.setText(oldINRValue+"");
            }

            newINRInput = (EditText)findViewById(R.id.patient_newINR);
            if(newINRValue==0){
                newINRInput.setText("");
            }else{
            newINRInput.setText(newINRValue+"");}

            oldDosageInput = (EditText)findViewById(R.id.patient_previous_dosage);
            if (prevDosages==""){
                oldDosageInput.setText("");
            }else{
            oldDosageInput.setText(prevDosages);}

            if(drugPosition ==1){
                dTabLayout.getTabAt(1).select();
            }

            if(position==1){
                mTabLayout.getTabAt(1).select();
            }




        }

    }


    public void askConfirmation(final View view){

        PatientMedicalData patientMedicalData = getPatientDataFromUI();
        String id = patientMedicalData.getPatientID();
        int age = patientMedicalData.getAge();
        float oldINR = patientMedicalData.getOldINRValue();
        float newINR = patientMedicalData.getNewINRValue();
        String procedure = patientMedicalData.getProcedure();
        //float oldDose = patientMedicalData.getOldDose();
        String gender = patientMedicalData.getGender();
        String drugString = patientMedicalData.getDrug();


        if(flag==0) {
            //nextTest
            String nextTest = "";
            if(2.4<oldINRValue && oldINRValue<3.6 && 2.4<newINRValue && newINRValue<3.6){
                nextTest = "Test INR at 2-3 weeks";
            }
            else{
                nextTest = "Test INR between 4-7 days";
            }

            final AlertDialog.Builder nextTestAlert = new AlertDialog.Builder(this)
                    .setTitle(nextTest)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            predictDosage(view);
                        }
                    });


            //confirmation

            new AlertDialog.Builder(this)
                    //.setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Confirm Values")
                    .setMessage(
                            "Patient ID : " + id + "\n" +
                            "Patient Age : " + age + "\n" +
                            "Gender : " + gender + "\n" +
                            "Drug : " + drugString + "\n" +
                            "Procedure : " + procedure + "\n" +
                            "Old INR : " + oldINR + "\n" +
                            "New INR : " + newINR + "\n" +
                            "Previous Dosages : " + prevDosages)
                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //loadMain.setVisibility(View.VISIBLE);
                            nextTestAlert.show();
                        }
                    })
                    .setNegativeButton("Change", null)
                    .show();


        }
    }


    public void predictDosage(View view) {


        boolean netcheck=isNetworkConnected();
        Log.i(getLogAppender(),"Net has been checked");
        if(!netcheck){
            loadMain.setVisibility(View.GONE);
            Log.i(getLogAppender(),"No internet");
            Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show();
            Log.i(getLogAppender(),"Dialog created n shown");
        }
        else {
            Log.i(getLogAppender(),"There is internet");

            Log.i(getLogAppender(), "Prediction request initialized");
            try {
                Patient patient = getPatient();
                if (flag==0) {
                    loadMain.setVisibility(View.VISIBLE);
                    JsonObjectRequest dosagePredictionRequest = makeDosagePredictionRequest(patient);
                    RequestQueueSingleton.getInstance(this).addToRequestQueue(dosagePredictionRequest);
                }
            } catch (JSONException e) {
                loadMain.setVisibility(View.GONE);
                Log.i(logAppender, "Error parsing Input");
                e.printStackTrace();
            }

        }
    }

    private Patient getPatient() {
        PatientMedicalData patientMedicalData = getPatientDataFromUI();
        Patient.getInstance().setPatientMedicalData(patientMedicalData);
        return Patient.getInstance();
    }

    private JsonObjectRequest makeDosagePredictionRequest(final Patient patient) throws JSONException {

        String dosagePredictionServerURL = "https://warfarin-predictor.herokuapp.com/predictor";
        JsonObjectRequest dosagePredictionRequest = new JsonObjectRequest(Request.Method.POST,
                    dosagePredictionServerURL, patient.getPatientMedicalDataJSON(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(logAppender, "Response received");
                    String predictedDosage = response.optString("response");
                    if(drugPosition ==1){
                        double acitromDose = Double.parseDouble(predictedDosage)/2;
                        predictedDosage = String.valueOf(acitromDose);
                    }
                    Log.d(logAppender,"Predicted dosage is " + predictedDosage);
                    System.out.println(response.toString());
                    //check for negative predocted value of dosage
                   // int pd = Integer.parseInt(predictedDosage);
                    if (predictedDosage.charAt(0) == '-' && newINRValue>=2 && newINRValue<=4){
                       Toast negToast = Toast.makeText(getApplicationContext(),"The App cannot help you. Contact Physician",Toast.LENGTH_LONG );
                        //meetDocToast.setDuration(10);
                        negToast.show();
                        //Intent i=new Intent(MainActivity.this,MainActivity.class);
                        //finish();
                        //overridePendingTransition(0,0);
                        //startActivity(i);
                        //overridePendingTransition(0,0);
                        Log.i(logAppender,"Negative output");
                        flag=1;
                    }
                      Log.i(logAppender,"Going to check for flag "+flag);
                    // Insert flag variable here to check for error
                    if (flag==0){
                        Log.i(logAppender,"Checked flag");
                        Intent resultIntent = new Intent(getBaseContext(),ResultActivity.class);
                    resultIntent.putExtra("dosage",predictedDosage);
                    resultIntent.putExtra("drugID", drugPosition);
                    startActivity(resultIntent);}
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadMain.setVisibility(View.GONE);
                    Log.d(logAppender,"Prediction Error");
                    error.printStackTrace();
                    displayPredictionError(error);
                }
            }){
            @Override
            public Map<String,String> getHeaders(){
                Map<String,String> headers = new HashMap<String, String>();
                String authorizationToken = "Token "+patient.getAuthorizationToken();
                headers.put("Authorization",authorizationToken);
                return headers;
            }
        };

        return dosagePredictionRequest;

    }

    private void displayPredictionError(final VolleyError error){
        if(error instanceof TimeoutError){
            Toast timeoutToast = Toast.makeText(this,"Request timed out, " +
                    "please try again", Toast.LENGTH_LONG);
            timeoutToast.show();
        }
    }


    public void logout_main(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
// Add the buttons
        builder.setMessage("Do you want to logout ?");
        builder.setPositiveButton("Log Out", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Log.i(logAppender,"Going back to login activity");
                Intent intent = new Intent(getBaseContext(),LoginActivity.class);
                startActivity(intent);
                Log.i(logAppender,"Came back to login activity");
                Log.i(logAppender,"Changed to null vals");
            }
        });
        builder.setNegativeButton("Stay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
             //   Intent intent = new Intent(getBaseContext(),MainActivity.class);
             //   startActivity(intent);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
        /* Log.i(logAppender,"Going back to login activity");
        Intent intent = new Intent(getBaseContext(),LoginActivity.class);
        startActivity(intent);
        Log.i(logAppender,"Came back to login activity");
        //EditText pwd = (EditText)findViewById(R.id.password);
        //EditText uname = (EditText)findViewById(R.id.Username);
        //pwd.getText().clear();
        //uname.getText().clear();
        Log.i(logAppender,"CHanged to null vals");
        */
    }

    public void onBackPressed(){

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
// Add the buttons
        builder.setMessage("Do you want to logout ?");
        builder.setPositiveButton("Log Out", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Log.i(logAppender,"Going back to login activity");
                Intent intent = new Intent(getBaseContext(),LoginActivity.class);
                startActivity(intent);
                Log.i(logAppender,"Came back to login activity");
                Log.i(logAppender,"Changed to null vals");
            }
        });
        builder.setNegativeButton("Stay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              //  Intent intent = new Intent(getBaseContext(),MainActivity.class);
              //  startActivity(intent);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private PatientMedicalData getPatientDataFromUI(){

        String alertMessage="";
        /*
        Toast ageToast = Toast.makeText(this,"Please enter age",Toast.LENGTH_LONG );
        Toast oldinrToast = Toast.makeText(this,"Please enter old INR value",Toast.LENGTH_LONG );
        Toast newinrToast = Toast.makeText(this,"Please enter new INR value",Toast.LENGTH_LONG );
        Toast olddosageToast = Toast.makeText(this,"Please enter old Dosage value",Toast.LENGTH_LONG );
        */
        flag = 0;





        EditText oldDosageInput = (EditText)findViewById(R.id.patient_previous_dosage);
        boolean flag4=oldDosageInput.getText().toString().isEmpty();

        if(!flag4) {
            oldDosage =0;
            try {
                prevDosages = oldDosageInput.getText().toString();
                String[] dosageString = prevDosages.split(" ");
                long dosSum=0;
                int count=0;
                //for(String s : dosageString){
                for(int i=0;i<dosageString.length;i++){
                    int dos;
                    dos = Integer.parseInt(dosageString[i]);
                    if (dos < 1 || dos > 20 || dosageString.length<1||dosageString.length>7) {
                        //Toast meetDocToast = Toast.makeText(this, "Enter Valid Dosage Values", Toast.LENGTH_LONG);
                        //meetDocToast.show();
                        alertMessage = "Please Enter Up To 7 Valid Dosage Values";
                        flag = 1;
                        break;
                    }
                    dosSum += dos;
                    count++;
                    //Make oldDosage into a 7 day sequence
                    if(i==dosageString.length-1 && count <7) {
                        i = -1;
                    }else if(count==7){
                        break;
                    }
                }
                //Toast.makeText(this, "dossum : "+ dosSum, Toast.LENGTH_SHORT).show();
                oldDosage=(float) dosSum/7;
                if(drugPosition ==1){
                    oldDosage=oldDosage*2;
                }


            }catch(Exception e){
                flag =1;
                Log.i(logAppender, e.toString());
                //Toast.makeText(this, "Enter Valid Dosage Values", Toast.LENGTH_LONG).show();
                alertMessage = "Please Enter Up To 7 Valid Dosage Values";
            }
        }
        else{
            flag=1;
            Log.i(logAppender, "Old Dosage value is null");
            alertMessage = "Please Enter Up To 7 Previous Dosage Values";
        }


        EditText newINRInput = (EditText)findViewById(R.id.patient_newINR);
        boolean flag3=newINRInput.getText().toString().isEmpty();
        if (!flag3){
            newINRValue = (float)Float.parseFloat(newINRInput.getText().toString());
        }
        else{
            flag=1;
            Log.i(logAppender, "New INR value is null");
            alertMessage ="Please enter new INR value";
        }


        EditText oldINRInput = (EditText)findViewById(R.id.patient_oldINR);
        boolean flag2=oldINRInput.getText().toString().isEmpty();
        if (!flag2){
            oldINRValue = (float)Float.parseFloat(oldINRInput.getText().toString());
        }
        else{
            flag=1;
            Log.i(logAppender, "Old INR value is null");
            alertMessage ="Please enter old INR value";
        }


        EditText ageInput = (EditText)findViewById(R.id.patient_age);
        boolean flag1=ageInput.getText().toString().isEmpty();
        if (!flag1){
            ageBox.setErrorEnabled(false);
            patientAge = Integer.parseInt(ageInput.getText().toString());
        }
        else{
            flag=1;

            ageBox.setError("Please enter valid Age.");

            Log.i(logAppender, "Please enter age");
            alertMessage ="Please enter age.";
        }



        EditText idInput = (EditText)findViewById(R.id.patient_id);
        boolean flag0=idInput.getText().toString().isEmpty();
        if (!flag0){
            idBox.setErrorEnabled(false);
            patientID = idInput.getText().toString();
            if(patientID.length()!=20){
                idBox.setError("Please enter valid ID.");
                flag=1;
                alertMessage ="Please enter valid Patient ID.";
            }
        }
        else{
            flag=1;

            idBox.setError("Please enter Patient ID.");

            Log.i(logAppender, "Please enter valid ID");
            alertMessage ="Please enter Patient ID.";
        }





        //Intent i=new Intent(MainActivity.this,MainActivity.class);
       /*
        if (!flag1){
            i.putExtra("age",ageInput.getText().toString());
        }
        else{
            i.putExtra("age","");
        }

        if (!flag2){
            i.putExtra("oldinr",oldINRInput.getText().toString());
        }
        else{
            i.putExtra("oldinr","");
        }

        if (!flag3){
            i.putExtra("newinr",newINRInput.getText().toString());
        }
        else{
            i.putExtra("newinr","");
        }

        if (!flag4){
            i.putExtra("olddose",oldDosageInput.getText().toString());
        }
        else{
            i.putExtra("olddose","");
        }

        //finish();
        //overridePendingTransition(0,0);
        //startActivity(i);
        //overridePendingTransition(0,0);       */

        AlertDialog.Builder alert = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(alertMessage)
                .setPositiveButton("OK", null);

        if(flag==1){
            alert.show();
        }

        if(flag==0) {
            if (newINRValue < 2 || newINRValue > 4) {
                flag = 1;
                alertMessage = "Contact Your Physician";
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(alertMessage)
                        .setPositiveButton("OK", null).show();
            }
        }



/*
        int patientAge=0;
        EditText ageInput = (EditText)findViewById(R.id.patient_age);
        if (ageInput.getText().toString().isEmpty()){
            Toast ageToast = Toast.makeText(this,"Please enter age",Toast.LENGTH_LONG );
            Log.i(logAppender, "Please enter age");
            //meetDocToast.setDuration(10);
            //ageToast.show();
            Intent i=new Intent(MainActivity.this,MainActivity.class);
            /*if (!flag2){
                i.putExtra("oldinr",oldINRInput.getText().toString());
            }
            else{
                i.putExtra("oldinr","");
            }
            finish();
            overridePendingTransition(0,0);
            startActivity(i);
            overridePendingTransition(0,0);
            ageToast.show();
            flag=1;
        }

     else{
         patientAge = Integer.parseInt(ageInput.getText().toString());}


        EditText oldINRInput = (EditText)findViewById(R.id.patient_oldINR);
        long oldINRValue = 0;

        if (oldINRInput.getText().toString().isEmpty()){
            Toast oldinrToast = Toast.makeText(this,"Please enter old INR value",Toast.LENGTH_LONG );
            Log.i(logAppender, "Old INR value is null");
            Intent i=new Intent(MainActivity.this,MainActivity.class);
            finish();
            overridePendingTransition(0,0);
            startActivity(i);
            overridePendingTransition(0,0);
            oldinrToast.show();
            flag=1;
        }

        else{
            oldINRValue = (long)Float.parseFloat(oldINRInput.getText().toString());}

        EditText newINRInput = (EditText)findViewById(R.id.patient_newINR);
        long newINRValue = 0;

        if (newINRInput.getText().toString().isEmpty()){
            Toast newinrToast = Toast.makeText(this,"Please enter new INR value",Toast.LENGTH_LONG );
            Log.i(logAppender, "New INR value is null");
            Intent i=new Intent(MainActivity.this,MainActivity.class);
            finish();
            overridePendingTransition(0,0);
            startActivity(i);
            overridePendingTransition(0,0);
            newinrToast.show();
            flag=1;
        }

        else{
            newINRValue = (long)Float.parseFloat(newINRInput.getText().toString());}

        if (newINRValue<2 || newINRValue>5){
            Toast meetDocToast = Toast.makeText(this,"Contact Your Physician",Toast.LENGTH_LONG );
            //meetDocToast.setDuration(10);
            meetDocToast.show();
            Intent i=new Intent(MainActivity.this,MainActivity.class);
            finish();
            overridePendingTransition(0,0);
            startActivity(i);
            overridePendingTransition(0,0);
            flag=1;
        }


        EditText oldDosageInput = (EditText)findViewById(R.id.patient_previous_dosage);
        long oldDosage = 0;

        if (oldDosageInput.getText().toString().isEmpty()){
            Toast olddosageToast = Toast.makeText(this,"Please enter old Dosage value",Toast.LENGTH_LONG );
            Log.i(logAppender, "Old Dosage value is null");
            Intent i=new Intent(MainActivity.this,MainActivity.class);
            finish();
            overridePendingTransition(0,0);
            startActivity(i);
            overridePendingTransition(0,0);
            olddosageToast.show();
            flag=1;
        }

        else{
            oldDosage = (long)Float.parseFloat(oldDosageInput.getText().toString());}
*/
        String gender = null;
        if(position == 0){
            gender = "Male";
        }else{
            gender = "Female";
        }


        if(drugPosition ==1){
            drug="Acitrom";
        }else{
            drug ="Warfarin";
        }



        Spinner procedureInput = (Spinner)findViewById(R.id.patient_procedure);
        String procedureType = procedureInput.getItemAtPosition(procedureInput.getSelectedItemPosition()).toString();

        PatientMedicalData patientWarfarinMedicalData = PatientWarfarinMedicalData.Builder.newInstance()
                .setPatientID(patientID)
                .setAge(patientAge)
                .setGender(gender)
                .setOldINRValue(oldINRValue)
                .setNewINRValue(newINRValue)
                .setOldDose(oldDosage)
                .setProcedure(procedureType)
                .setDrug(drug)
                .build();

        return patientWarfarinMedicalData;

    }

    protected String getLogAppender() {
        return logAppender;
    }

    private boolean isNetworkConnected() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return connected;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        flag=0;
        switch (keyCode) {

            case KeyEvent.KEYCODE_ENTER:
                boolean netcheck=isNetworkConnected();
                Log.i(getLogAppender(),"Net has been checked");
                if(!netcheck){
                    Log.i(getLogAppender(),"No internet");
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Net Connection Is Not Present");
                    builder.setPositiveButton("Refresh", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(getBaseContext(),MainActivity.class);
                            startActivity(intent);
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    Log.i(getLogAppender(),"Dialog created n shown");
                }
                else {
                    Log.i(getLogAppender(),"There is internet");

                    Log.i(getLogAppender(), "Prediction request initialized");
                    try {
                        Patient patient = getPatient();
                        JsonObjectRequest dosagePredictionRequest = makeDosagePredictionRequest(patient);
                        RequestQueueSingleton.getInstance(this).addToRequestQueue(dosagePredictionRequest);
                    } catch (JSONException e) {
                        Log.i(logAppender, "Error parsing Input");
                        e.printStackTrace();
                    }

                }
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

}
