package com.example.amruth.warfarinpredictorclient.patient;

import org.json.JSONException;
import org.json.JSONObject;

public interface PatientMedicalData {

    public String getGender();

    public String getPatientID();

    public String getProcedure();

    public int getAge();

    public float getOldINRValue();

    public float getNewINRValue();

    public float getOldDose();

    public String getDrug();

}
