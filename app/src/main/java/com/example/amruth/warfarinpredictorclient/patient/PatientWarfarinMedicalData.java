package com.example.amruth.warfarinpredictorclient.patient;


import org.json.JSONException;
import org.json.JSONObject;

public class PatientWarfarinMedicalData implements PatientMedicalData {

        private String gender,procedure,drug,patientID;
        private int age;
        private float oldINRValue, newINRValue;
        private float oldDose;

        public PatientWarfarinMedicalData(Builder builder){
            this.gender = builder.gender;
            this.procedure = builder.procedure;
            this.age = builder.age;
            this.oldINRValue = builder.oldINRValue;
            this.newINRValue = builder.newINRValue;
            this.oldDose = builder.oldDose;
            this.drug = builder.drug;
            this.patientID = builder.patientID;
        }

    public String getGender() {
        return gender;
    }

    public String getPatientID() {
        return patientID;
    }

    public String getDrug() {
        return drug;
    }

    public String getProcedure() {
        return procedure;
    }

    public int getAge() {
        return age;
    }

    public float getOldINRValue() {
        return oldINRValue;
    }

    public float getNewINRValue() {
        return newINRValue;
    }

    public float getOldDose() {
        return oldDose;
    }

    public static class Builder{
            private String gender,procedure,drug,patientID;
            private int age;
            private float oldINRValue, newINRValue;
            private float oldDose;

            private Builder() {}

            public static Builder newInstance(){
                return new Builder();
            }

            public Builder setGender(String gender){
                this.gender = gender;
                return this;
            }

            public Builder setPatientID(String patientID){
                this.patientID = patientID;
                return this;
            }

            public Builder setAge(int age){
                this.age = age;
                return this;
            }

            public Builder setProcedure(String procedure){
                this.procedure = procedure;
                return this;
            }
            public Builder setDrug(String drug){
                this.drug = drug;
                return this;
            }

            public Builder setOldINRValue(float oldINRValue){
                this.oldINRValue = oldINRValue;
                return this;
            }

            public Builder setNewINRValue(float newINRValue){
                this.newINRValue = newINRValue;
                return this;
            }

            public Builder setOldDose(float oldDose){
                this.oldDose = oldDose;
                return this;
            }

            public PatientWarfarinMedicalData build(){
                return new PatientWarfarinMedicalData(this);
            }
        }
}
